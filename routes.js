import difficulty from "./assets/javaScript/Difficulty.js";
import quiz from "./assets/javaScript/quiz.js";
import slider from "./assets/javaScript/slider.js";
import score from "./assets/javaScript/score.js"

export const routes = {
    'difficulty': difficulty,
    '/': slider,
    'quiz': quiz,
    'score': score,
};