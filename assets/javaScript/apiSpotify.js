

const APIController = (function(){

    // methode privé
    const _getToken = async () => {
        const result = await fetch('https://accounts.spotify.com/api/token',{
            method:'POST',
            headers: {'Content-Type' : 'application/x-www-form-urlencoded',
                      'Authorization' : 'Basic ODZlMWI4ZTdlNjEwNGFiM2IyYjEyNDEwNzhhNWQ4NTU6NjA2ZjYzMTg4Njk2NDE3NGFjMmI5ZTY2NTM4YTRmOTg='
 
            },
            body: new URLSearchParams({
                "grant_type": "client_credentials"
            })
            
        });
        const data = await result.json();
        // console.log(data);
        return data.access_token;
}

const _getPlaylist = async (token, playlistId) => {

    const limit = 10;

    const result = await fetch(` https://api.spotify.com/v1/playlists/${playlistId}?limit=${limit}&locale=fr_FR`,{
        method:'GET',
        headers: {'Authorization' : 'Bearer ' + token}
});
const data = await result.json();
        return data;
}

// methode public

return {
    getToken(){
        return _getToken();
    },
    getPlaylist(token, playlist){
        return _getPlaylist(token, playlist);
    }
}
})();

function randomIntFromInterval(min, max) { // min and max included 
    return Math.floor(Math.random() * (max - min + 1) + min)
  }
  

let token = await APIController.getToken();
let playlist = await APIController.getPlaylist(token, '4l1CEhc7ZPbaEtiPdCSGbl')
let items = playlist.tracks.items;

let item = items[randomIntFromInterval(0, 80)]


// console.log(item.track.artists[0].name);
// console.log(item.track.album.name);
// // console.log(item.track.artists[0].name);


let albumImage = document.getElementById("coverAlbum");
let elementArtist = document.getElementById("nameArtist");
let elementAlbum = document.getElementById("nameAlbum");
// console.log(item.track.album.images[0].url);

albumImage.src = item.track.album.images[0].url
let nameArtist = item.track.artists[0].name
let nameAlbum = item.track.album.name

localStorage.setItem('nameArtist', JSON.stringify(nameArtist))
localStorage.setItem('nameAlbum', JSON.stringify(nameAlbum))

