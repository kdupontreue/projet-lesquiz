export default {
    view: async () => {
        return `
<section id="welcomePage" class="">
    <div class="firstCircle">
        <img src="../../assets/img/firstCircleWhite.svg" alt="Image de fond du site">
    </div>
    
    <div class="secondCircle">
        <img src="../../assets/img/secondCircleWhite.svg" alt="Image de fond du site">
    </div>
    
    <form class="pictureAlbum">
        <li class="firstAlbum">
            <img src="../../assets/img/liveLoveAsap.jpg" alt="Album de Asap">
        </li>
        <li class="secondAlbum">
            <img src="../../assets/img/kendrickLamar.jpg" alt="Album de Kendrick">
        </li>
        <li class="thirdAlbum">
            <img src="../../assets/img/kaaris.jpg" alt="Album de Kaaris">
        </li>
        <li class="fourAlbum">
            <img src="../../assets/img/gazo.jpg" alt="Album de Gazo">
        </li>
    </form>
    
    
    <div class="titleMain">
        <div class="title">
            <h1>Le Squizzz</h1>
            <img src="../../assets/img/logoLeSquizz.png" alt="Logo le squizz">
        </div>  
        <div>
            <a href="../#slider"><button id="buttonWelcome" class="buttonSwitchSlide">SQUEZZOS</button></a>
        </div>
    </div>
</section>
    
`
    },
    after: () => {
        console.log("Component Home mounted");
               }
};
